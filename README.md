## technologies

* node.js,
* express,
* ejs,
* MongoDB, 
* mongoose, 
* bodyParser, 
* loadash, 

## requirements
  node.js

  MongoDB or mongo in docker 

  if you have docker you can run :

  ```
    npm run mongo-docker
  ```
## instalation
```
  npm i
  npm run start
  ```
